package com.nico.springbootmails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class SendEmailService {

	@Autowired
	JavaMailSender javaMailSender;
	
	public void sendEmail(String to, String body, String topic) {
		
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setTo(to); // Puede recibir un array y en ese caso lo manda a varios destinatarios.
		simpleMailMessage.setSubject(topic);
		simpleMailMessage.setText(body);
		javaMailSender.send(simpleMailMessage);
		
		System.out.println("Sent email...");;
	}
	
	
}
