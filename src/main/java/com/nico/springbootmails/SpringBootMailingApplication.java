package com.nico.springbootmails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class SpringBootMailingApplication {

	@Autowired 
	private SendEmailService sendEmailService;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootMailingApplication.class, args);
	}

	
	@EventListener(ApplicationReadyEvent.class) // Se ejecuta este metodo cuando ocurre el evento ApplicationReady, es decir, al levantar la app.
	public void triggerWhenStarts() {
		sendEmailService.sendEmail("mail_toe@hotmail.com", "HOlaaa como andas brodilovi", "Este es el titulo paaahh");
	}
	
}
